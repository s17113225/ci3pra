<?php
defined("BASEPATH") or exit("No direct script access allowed");

class Practice extends MY_infrastructure
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model("Practice_model", "model", TRUE);
    }

    public function index()
    {
		if (!$this->getLogin()) {
			redirect(base_url("Login"));
		} else {
			$data = $this->viewItem();
			$data['header'] = '歡迎來到CodeIgniter的世界，實驗室的案子維護就靠你們了！';
			$data['title'] = '練習';
			$this->load->view("Home", $data);
		}
        
	}
	
	public function getTableData()
	{
		$result = $this->model->getTableData();
		
		$data = array('status' => 1, 'data' => $result->result_array());
		echo json_encode($data);
	}

	public function deleteData()
	{
		$key = $this->xss(json_decode($_POST["data"], true));

		$this->model->deleteData($key["b_id"]);

		$data = array('status' => 1);
		echo json_encode($data);
	}

	public function getModifyData()
	{
		$key = $this->xss(json_decode($_POST["data"], true));

		$result = $this->model->getModifyData($key["b_id"]);

		$data = array('status' => 1, 'data' => $result->result_array());
		echo json_encode($data);
	}

	public function modifyData()
	{
		$data = $this->xss(json_decode($_POST["data"], true));

		$this->model->modifyData($data);

		$data = array('status' => '1');
		echo json_encode($data);
	}

}
