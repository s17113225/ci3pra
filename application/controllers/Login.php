<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Login extends MY_infrastructure
{

	/**
	 * 建構載入需要預先執行的項目
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->model("Login_model", "model", TRUE);
	}

	public function index()
	{
		if ($this->getLogin()) {
			redirect(base_url("Practice"));
		} else {
			$data = $this->viewItem();
			$data['title'] = '登入';
			$this->load->view("Login", $data);
		}
	}

	/***
	傳入值為：$_Post['account','password','token']
	傳入驗證：資料消毒
	未填寫echo 0; 失敗echo 1; 偽造請求 echo2;成功 echo3;
	 ***/
	public function checkLogin()
	{
		$data = $this->xss(json_decode($_POST["data"], true));

		$account = $data['account'];
		$password = $data['password']; 

		$user_data = $this->model->checkLogin($account, $password); 
		if ($user_data) {
			if ($user_data->num_rows() == 0) {
				$data = array('status' => 0, 'data' => array());
			} else {
				$data = array('status' => 1, 'data' => array(), 'key' => $user_data->row()->key);
				$this->model->setSession($user_data->row());
			}
		} else {
			$data = array('status' => 2, 'data' => array());
		}
		echo json_encode($data);
	}

	public function out()
	{
		session_destroy();
		echo json_encode(array('status' => '1'));
	}
}
