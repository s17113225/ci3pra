<?php

class Practice_model extends CI_Model
{

	//DB操作手冊
	//https://codeigniter.org.tw/user_guide/database/active_record.html
	public function __destruct()
	{
		$this->db->close();
	}

	function getTableData()
	{
		$this->db->select('*');
		$this->db->from('blogs');
		$this->db->where('sha1(m_id)',$_SESSION['key']);
		$result = $this->db->get();
		return $result;
	}

	function deleteData($key)
	{
		$this->db->where('`b_id`', $key);
		$this->db->delete('blogs');
		return true;
	}

	function getModifyData($key)
	{
		$this->db->select('*');
		$this->db->from('blogs');
		$this->db->where('b_id', $key);
		$result = $this->db->get();
		return $result;
	}

	function modifyData($data)
	{
		$this->db->set('b_title', $data['b_title']);
		$this->db->set('b_content', $data['b_content']);
		$this->db->where('`b_id`', $data['b_id']);
		$this->db->update('blogs');

		$this->db->flush_cache();
	}

}
