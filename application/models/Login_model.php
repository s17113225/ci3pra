<?php

class Login_model extends CI_Model
{

	//DB操作手冊
	//https://codeigniter.org.tw/user_guide/database/active_record.html
	public function __destruct()
	{
		$this->db->close();
	}

	function checkLogin($account, $password)
	{
		$this->db->select('SHA1(`members`.`m_id`) as `key`,
                           members.`m_account` as `account`');
		$this->db->from('members');
		$this->db->where('m_account', $account);
		$this->db->where('m_password', sha1($password));
		$result = $this->db->get();
		return $result;

	}

	function setSession($dataObject)
	{
		$_SESSION['key'] = $dataObject->key;
		$_SESSION['account'] = $dataObject->account;
	}
}
