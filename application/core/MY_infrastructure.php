<?php
class MY_infrastructure extends CI_Controller {

	//false=未登入 true=已登入
	private $login;
	//private $menu;
	//private $permissionKey;
	private $account;
	private $key;

	
	public function __construct(){
		parent::__construct();
		// $this->serverFunction();
		$this->startSession(8*3600);		
		header("X-Frame-Options: DENY");
		$this->login = isset($_SESSION['key']);
		if($this->login){
			$this->key = $_SESSION['key'];
			$this->account = $_SESSION['account'];
		}
	}

	function startSession($expire = 0){
		//如果$expire設定為0的話以PHP.INI裡設定的時間為主
		if ($expire == 0){
			$expire = ini_get('session.gc_maxlifetime');
		}else{
			ini_set('session.gc_maxlifetime', $expire);
		}
		//如果$_COOKIE['PHPSESSID']不存在,在指定時間內SESSION過期
		if (empty($_COOKIE['PHPSESSID'])) {
			session_set_cookie_params($expire);
			session_start();
		} else {
			//如果$_COOKIE['PHPSESSID']存在
			session_start();
			setcookie('PHPSESSID', session_id(), time() + $expire);
		}
	}
		
	/**
	 * Https轉址
	 */
	private function serverFunction(){
		if(!((isset($_SERVER['HTTPS'])&&$_SERVER['HTTPS']=='on')||(isset($_SERVER['HTTP_X_FORWARDED_PROTO'])&&$_SERVER['HTTP_X_FORWARDED_PROTO']=='https'))){
			Header("HTTP/1.1 301 Moved Permanently");
			if(preg_match("/www/i", $_SERVER['SERVER_NAME'])){
			header('Location: https://XXXX.com'.$_SERVER['REQUEST_URI']);
			}else{
			header('Location: https://'.$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI']);
			}
			exit();
		}else{
			if(preg_match("/www/i", $_SERVER['SERVER_NAME'])){
			Header("HTTP/1.1 301 Moved Permanently");
			header('Location: https://XXXX.com'.$_SERVER['REQUEST_URI']);
			exit();
			}
		}
	}

	/**
	 * 回傳是否擁有登入狀態
	 * @return Boolean
	 */
	public function getLogin(){
		return $this->login;
	}

	/**
	 * 回傳用戶主鍵
	 * @return Int
	 */
	public function getkey(){
		return $this->key;
	}

	/**
	 * 回傳用戶主鍵
	 * @return Int
	 */
	public function getAccount()
	{
		return $this->account;
	}

	/**
	 * 替傳入陣列消毒，回傳消毒完成的結果
	 * @param  Array
	 * @return Array
	 */
	public function xss(array $array){

		foreach ($array as &$value){
			if(is_array($value)){
				$value = $this->xss($value);
			}else{
				$value = $this->security->xss_clean($value);
			}
		}
		
		return $array;
	}

	public function viewItem()
	{
		$data['key'] = $this->key;
		$data['account'] = $this->account;
		return $data;
	}

}

?>
