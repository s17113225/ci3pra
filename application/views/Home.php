<html>

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>CodeIgniter-<?php echo $title ?></title>

	<link href="<?php echo base_url() ?>assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">

	<script type="text/javascript" src="<?php echo base_url() ?>assets/js/jquery.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url() ?>assets/js/sweetalert.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url() ?>assets/js/popper.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url() ?>assets/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url() ?>assets/js/jquery.dcjqaccordion.2.7.js"></script>
</head>

<body>

	<style>
		* {
			font-family: '微軟正黑體', 'Arial' !important;
		}
	</style>

	<h1><?php echo $header ?></h1>

	<table>
		<thead>
			<tr>
				<th>操作</th>
				<th>編號</th>
				<th>標題</th>
				<th>內容</th>
			</tr>
		</thead>
		<tbody id='practiceTable'>

		</tbody>
	</table>

	<div class="modal fade" id="modifyModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title">修改資料</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<form id="modifyForm" method="post">
						<div class="row">
							<div class="form-group  col-md-6">
								<label>文章標題</label>
								<input name="b_title" type="text" class="form-control" placeholder="Title">
							</div>

							<div class="form-group  col-md-12">
								<label>文章內容</label>
								<textarea name="b_content" class="form-control" cols="60" rows="5" style="margin-top: 0px; margin-bottom: 0px; height: 115px;"></textarea>
							</div>
							<div class="form-group  col-md-3" style="display: none;">
								<label>文章id</label>
								<input name="b_id" type="number" class="form-control" placeholder="key">
							</div>
						</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">
						關閉
					</button>
					<button type="submit" class="btn btn-primary">
						新增
					</button>
				</div>
				</form>
			</div>
		</div>
	</div>

	<script type="text/javascript">
		$(document).ready(function() {
			getTableData();

			/**
			 * 送出更改表單，並呼叫modifyData function
			 */
			$("form[id='modifyForm']").submit(function(e) {
				e.preventDefault();
				modifyData($("#modifyForm"));
			});

		});

		let base_url = (str = "") => '<?php echo base_url() ?>' + str;

		$.fn.getFormObject = function() {
			var obj = {};
			var arr = this.serializeArray();
			arr.forEach(function(item, index) {
				if (obj[item.name] === undefined) { // New
					obj[item.name] = item.value || ' ';
				} else { // Existing
					if (!obj[item.name].push) {
						obj[item.name] = [obj[item.name]];
					}
					obj[item.name].push(item.value || '');
				}
			});
			return obj;
		};



		let getTableData = () => {

			$.ajax({
					type: 'POST',
					url: base_url('Practice/getTableData'),
				})
				.done((e) => {

					e = JSON.parse(e);

					let str = "";
					for (const {
							b_id,
							b_title,
							b_content
						} of e.data) {
						str += `
								<tr>
									<td>
										<button type="button" class="btn btn-outline-primary" onclick="getModifyData('${b_id}')">修改</button>
										<button type="button" class="btn btn-outline-danger" onclick="deleteData('${b_id}')">刪除</button>
									</td>
									<td>${b_id}</td>
									<td>${b_title}</td>
									<td>${b_content}</td>
								</tr>
								`;
					}

					$('#practiceTable').html(str);
				})
				.fail((e) => {
					swal("失敗", "操作失敗，請重新執行", "error");
					return true;
				})
		}

		let deleteData = b_id => {

			let data = {
				"b_id": b_id
			}


			$.ajax({
					type: 'POST',
					url: base_url('Practice/deleteData'),
					dataType: 'json',
					data: {
						data: JSON.stringify(data)
					}
				})
				.done((e) => {
					getTableData();
				})
				.fail((e) => {
					swal("失敗", "操作失敗，請重新執行", "error");
					return true;
				})

		}

		let getModifyData = b_id => {

			let data = {
				"b_id": b_id
			}


			$.ajax({
					type: 'POST',
					url: base_url('Practice/getModifyData'),
					dataType: 'json',
					data: {
						data: JSON.stringify(data)
					}
				})
				.done((e) => {
					console.log(Object.keys(e.data[0]).length);


					for (let i = 0; i < Object.keys(e.data[0]).length; i++) {

						$("#modifyForm > div > div > input[name = '" + Object.keys(e.data[0])[i] + "']").val(Object.values(e.data[0])[i]);
						$("#modifyForm > div > div > textarea[name = '" + Object.keys(e.data[0])[i] + "']").val(Object.values(e.data[0])[i]);
					};
					// $("input[name = '" + Object.keys(e.data)[i] + "']").val(Object.values(e.data)[i]);
					$('#modifyModal').modal('show');


				})
				.fail((e) => {
					swal("失敗", "操作失敗，請重新執行", "error");
					return true;
				})

		}



		/**
		 * 更改資料
		 * @param  Object $formDom
		 * @return bool
		 */
		let modifyData = $formDom => {
			swal("確定要修改嗎?", {
				icon: "warning",
				buttons: true,
				dangerMode: true,
			}).then(function(value) {
				if (value) {
					let data = $formDom.getFormObject();

					$.ajax({
							type: 'POST',
							url: base_url('Practice/modifyData'),
							dataType: 'json',
							data: {
								data: JSON.stringify(data)
							}
						})
						.done((e) => {
							if (e.status == 1) {
								swal("成功", "資料修改成功", "success");
								getTableData();
								$('#modifyModal').modal('hide');
							} else {
								swal("失敗", "資料修改失敗，請重新再試", "error");
								return true;
							}
						})
						.fail((e) => {
							swal("失敗", "操作失敗，請重新執行", "error");
							return true;
						})
				};
			});
		}
	</script>

</body>


</html>
