<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>CodeIgniter練習 - <?php echo $title ?></title>
	<link rel="shortcut icon" type="image/x-icon" href="Vertical/favicon.ico">
	<!-- google font -->
	<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
	<link href="<?php echo base_url() ?>assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url() ?>assets/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url() ?>assets/css/ionicons.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url() ?>assets/css/simple-line-icons.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url() ?>assets/css/jquery.mCustomScrollbar.css" rel="stylesheet">
	<link href="<?php echo base_url() ?>assets/css/style.css" rel="stylesheet">
	<link href="<?php echo base_url() ?>assets/css/responsive.css" rel="stylesheet">

	<script type="text/javascript" src="<?php echo base_url() ?>assets/js/jquery.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url() ?>assets/js/sweetalert.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url() ?>assets/js/popper.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url() ?>assets/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url() ?>assets/js/jquery.dcjqaccordion.2.7.js"></script>

</head>

<body class="bg_darck">
	<div class="sufee-login d-flex align-content-center flex-wrap">
		<div class="container">
			<div class="login-content">
				<div class="login-form">
					<form id="loginForm" method="post">
						<div class="form-group">
							<label>帳號</label>
							<input name="account" class="form-control" placeholder="Account">
						</div>
						<div class="form-group">
							<label>密碼</label>
							<input name="password" type="password" class="form-control" placeholder="Password">
						</div>
						<div class="checkbox">
							<!-- <label>
                    <input type="checkbox"> Remember Me
                </label> -->
						</div>
						<button type="submit" class="btn btn-success btn-flat m-b-30 m-t-30" style="margin-top:20px">登入</button>
						<!-- <div class="register-link m-t-15 text-center">
                <p>還沒註冊商家帳號嗎?
                    <a href="<?php echo base_url("Register") ?>">點這裡註冊</a>
                </p>
            </div> -->
					</form>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript">
		let base_url = (str = "") => '<?php echo base_url() ?>' + str;

		$.fn.getFormObject = function() {
			var obj = {};
			var arr = this.serializeArray();
			arr.forEach(function(item, index) {
				if (obj[item.name] === undefined) { // New
					obj[item.name] = item.value || ' ';
				} else { // Existing
					if (!obj[item.name].push) {
						obj[item.name] = [obj[item.name]];
					}
					obj[item.name].push(item.value || '');
				}
			});
			return obj;
		};

		$("form").submit(function(e) {
			e.preventDefault();
			login.submit();
		});

		var login = {
			formDom: $("#loginForm"),
			submit: function() {
				var data = $("#loginForm").getFormObject();
				if (this.checkForm(data)) return;

				$.ajax({
						type: 'POST',
						url: base_url('Login/checkLogin'),
						dataType: 'json',
						data: {
							data: JSON.stringify(data)
						}
					})
					.done((e) => {
						if (e.status == 1) {
							swal("成功", "登入成功，為您重新導向", "success").then((result) => {
								if (result) {
									window.location.reload();
								} else {
									window.location.reload();
								}
							});
							setTimeout(() => {
								window.location.reload();
							}, 2000);
						} else {
							swal("失敗", "帳號或密碼錯誤", "error");
							return true;
						}
						console.log(e);

					})
					.fail((e) => {
						swal("失敗", "帳號或密碼錯誤", "error");
						return true;
					})
			},
			checkForm: function(formData) {
				if (formData.account == "") {
					swal("錯誤", "帳號不可為空", "error");
					return true;
				} else if (formData.password == "") {
					swal("錯誤", "密碼不可為空", "error");
					return true;
				} else {
					return false;
				}
			}
		}
	</script>

</body>

</html>
